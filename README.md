
Akono is a NodeJS binding for Android+Kotlin.

# Building

This JNI library needs a special node building which is implemented in the `android-node-v8` repository.
You can download it by initializing the git submodule

`
git submodule init
git submodule update -f
`

Follow the instructions inside to generate the binaries and copy out the libraries

`
cp -r $REPO/deps/android-node-v8/taler-android-build/compiled/ $REPO/deps/
`

where `$REPO` is this repo directory.

Once you have the special node library you can now build it

`
./gradlew assemble
`

# Testing

With a device connected run:

`
./gradlew connectedAndroidTest
`


